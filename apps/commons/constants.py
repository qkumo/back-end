Text, Integer, Boolean = 'Text', 'Integer', 'Boolean'

FIELD_TYPES = (
    ('text', Text),
    ('number', Integer),
    ('checkbox', Boolean),
)
