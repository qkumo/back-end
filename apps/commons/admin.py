from django.contrib import admin

from apps.commons.models import CustomFieldValue, CustomField

# Register your models here.
admin.site.register([CustomField, CustomFieldValue])
