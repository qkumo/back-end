from django.contrib.contenttypes.models import ContentType
from django.db import models

from apps.commons.constants import FIELD_TYPES, Text

# Create your models here.


class BaseModel(models.Model):
    """Base model for this project."""
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    modified_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class CustomField(BaseModel):
    """
     A field abstract -- it describe what the field is.  There are one of these
     for each custom field the user configures.
    """
    limit = models.Q(app_label='sites', model='site') | \
        models.Q(app_label='sites', model='meter')

    name = models.CharField(max_length=75)
    model = models.ForeignKey(
        ContentType, on_delete=models.CASCADE,
        limit_choices_to=limit
    )
    field_type = models.CharField(
        max_length=10, choices=FIELD_TYPES, default=Text)
    company = models.ForeignKey(
        'company.Company', on_delete=models.CASCADE, related_name='custom_fields')
    country = models.ForeignKey(
        'region.Country', on_delete=models.CASCADE,
        related_name='custom_fields')

    def __str__(self):
        return self.name

    def get_value_for_object(self, obj):
        return CustomFieldValue.objects.get_or_create(field=self, object_id=obj.id)[0]


class CustomFieldValue(BaseModel):
    """
    A field instance -- contains the actual data.  There are many of these, for
    each value that corresponds to a CustomField for a given model.
    """
    field = models.ForeignKey(CustomField, related_name='value',
                              on_delete=models.CASCADE)
    value = models.CharField(max_length=255, blank=True, null=True)
    object_id = models.PositiveIntegerField()

    def __str__(self):
        return self.value or ""
