from django.core.mail import send_mail
from celery.decorators import task
from celery.utils.log import get_task_logger

from time import sleep

logger = get_task_logger(__name__)


@task(name="send_mail_to")
def send_mail_to(subject, message, sender, receivers):
    is_task_completed = False
    try:
        sleep(5)
        is_task_completed = True
    except Exception as err:
        error = str(err)
        logger.error(error)
    if is_task_completed:
        send_mail(subject, message, sender, receivers)
    else:
        send_mail(subject, message, sender, receivers)
    return('Password reset link sent!')
