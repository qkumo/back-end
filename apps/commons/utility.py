from datetime import date


import os
import uuid
import datetime

from django.utils import timezone


def is_past_date(date_):
    return date_ < date.today()


def get_today():
    return timezone.now().astimezone().date()


def get_uuid_filename(filename):
    """
    rename the file name to uuid4 and return the
    path
    """
    ext = filename.split('.')[-1]
    return "{}.{}".format(uuid.uuid4().hex, ext)


def get_upload_path(instance, filename):
    return os.path.join(f"uploads/{instance.__class__.__name__.lower()}",
                        get_uuid_filename(filename))


def combine_date_parts(year: int = 0, month: int = 0, day: int = 1):
    """
    Combines date parts and returns date
    :param year: Year int
    :param month: Month int
    :param day: Day int (defaults to 1)
    :return: datetime.date instance if valid and None if invalid
    """
    date_kwargs = {'day': day}

    if year:
        date_kwargs['year'] = year
    if month:
        date_kwargs['month'] = month

    try:
        return datetime.date(**date_kwargs)
    except (OverflowError, ValueError, TypeError):
        return None
