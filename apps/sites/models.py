from datetime import datetime

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.commons.models import BaseModel


class Meter(BaseModel):
    number = models.CharField(max_length=100)
    commodity = models.ForeignKey(
        'data.Commodity', null=True, on_delete=models.SET_NULL)
    hh = models.BooleanField(default=False)
    site = models.ForeignKey(
        'sites.Site', null=True, blank=True,
        on_delete=models.CASCADE, related_name='meters')
    company = models.ForeignKey(
        'company.Company', null=True, blank=True,
        on_delete=models.CASCADE, related_name='meters')

    def __str__(self):
        return str(self.number)

    class Meta:
        ordering = ['-id']


class Site(BaseModel):
    name = models.CharField(max_length=256)
    street = models.CharField(max_length=255)
    city = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=100)
    company = models.ForeignKey(
        'company.Company', null=True, on_delete=models.SET_NULL)
    country = models.ForeignKey(
        'region.Country', null=True, on_delete=models.SET_NULL)
    division = models.ForeignKey(
        'company.Devision', null=True, on_delete=models.SET_NULL)
    supplier = models.CharField(max_length=125, null=True, blank=True)
    dno = models.CharField(max_length=125, null=True, blank=True)
    legal_entity_name = models.CharField(max_length=125, null=True, blank=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return str(self.name)


@receiver(post_save, sender=Site)
def save_commodities(sender, instance, **kwargs):
    from apps.data.models import Commodity
    commodities = ['Gas', 'Power']
    for commodity in commodities:
        commodity, _ = Commodity.objects.get_or_create(
            name=commodity,
            site=instance,
            company=instance.company
        )
