from rest_framework import serializers
from apps.sites.models import *
from apps.data.serializers.commodity import *
from apps.company.serializers import *
from apps.region.serializers.country import *


class SiteSerializer(serializers.ModelSerializer):
    commodities = serializers.SerializerMethodField()

    class Meta:
        model = Site
        exclude = ['company']

    def get_fields(self):
        fields = super().get_fields()
        request = self.context.get('request')

        if request and request.method.lower() == 'get':
            fields['division'] = DevisionDataSerializer(context=self.context)
            fields['country'] = CountryDataSerializer(context=self.context)
        return fields

    def get_commodities(self, obj):
        return obj.commodities.all().values('id', 'name')


class SiteCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = "__all__"
