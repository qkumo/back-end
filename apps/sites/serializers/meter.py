from rest_framework import serializers

from apps.data.serializers.commodity import *
from apps.sites.models import *
from apps.sites.serializers.site import SiteSerializer


class MeterSerializer(serializers.ModelSerializer):
    commodity = CommoditySerializer()
    site = SiteSerializer()

    class Meta:
        model = Meter
        exclude = ['company']


class MeterCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meter
        fields = "__all__"
