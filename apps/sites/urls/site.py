from rest_framework.routers import DefaultRouter

from apps.sites.views.site import SiteViewSet

router = DefaultRouter()

router.register('', SiteViewSet, basename="sites")

urlpatterns = router.urls
