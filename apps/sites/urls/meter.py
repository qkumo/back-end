from rest_framework.routers import DefaultRouter

from apps.sites.views.meter import MeterViewSet

router = DefaultRouter()

router.register('', MeterViewSet, basename="meters")

urlpatterns = router.urls
