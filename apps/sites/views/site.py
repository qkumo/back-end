from rest_framework.response import Response
from rest_framework import status, viewsets, filters
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from django_filters.rest_framework import DjangoFilterBackend

from apps.company.models import *
from apps.sites.models import *
from apps.sites.serializers.site import *


class SiteViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Site.objects.all()
    serializer_class = SiteSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['city', 'street', 'name']
    filter_fields = ['division', 'country', 'company']

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return super().get_serializer_class()
        return SiteCreateSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        user = self.request.user
        try:
            profile = user.profile
            queryset = queryset.filter(company=profile.company)
        except Profile.DoesNotExist:
            queryset = queryset.none()

        return queryset
