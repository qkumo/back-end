from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.sites.models import *
from apps.sites.serializers import MeterCreateSerializer, MeterSerializer
from apps.company.models import Profile


class MeterViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Meter.objects.all()
    serializer_class = MeterSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['number']
    filter_fields = ['commodity', 'hh']

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return super().get_serializer_class()
        return MeterCreateSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        user = self.request.user
        try:
            profile = user.profile
            queryset = queryset.filter(company=profile.company)
        except Profile.DoesNotExist:
            queryset = queryset.none()

        return queryset
