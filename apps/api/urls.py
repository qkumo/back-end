from django.urls import include, path

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from rest_framework.routers import DefaultRouter
from apps.data.views.commodity import CommodityViewSet

app_name = "api"

router = DefaultRouter()
router.register('commodities', CommodityViewSet, basename='commodities')

urlpatterns = [

    path(r'upload/', include('apps.company.urls.upload'), name='upload'),
    path(r'divisons/', include('apps.company.urls.divison'), name='divison'),
    path(r'auth/', include('apps.auths.urls'), name='auth'),
    path(r'dashboard/', include('apps.dashboard.urls'), name='dashboard'),
    path(r'data/', include('apps.data.urls'), name='data'),
    path(r'sites/', include('apps.sites.urls.site'), name='site'),
    path(r'meters/', include('apps.sites.urls.meter'), name='meter'),
    path(r'invoice/', include('apps.invoice.urls.invoice_urls'), name='invoice'),
    path(r'countries/', include('apps.region.urls.country'), name='countries')
]
urlpatterns += router.urls
