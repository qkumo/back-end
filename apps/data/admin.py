from django.contrib import admin

from apps.data.models import *
from apps.sites.models import Meter

# Register your models here.


@admin.register(MeterRecord)
class MeterModelAdmin(admin.ModelAdmin):
    list_display = ('meter', 'value', 'date',
                    'mtype', 'origin_type')


admin.site.register(MeterDataFile)
