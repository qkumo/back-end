import csv
from io import StringIO
from datetime import datetime, timedelta
from django.apps import apps

from xlrd import open_workbook, xldate
from ..db.upload import upload_data
from apps.sites.models import *
# from data.models import *


def extract_date(exel_date, with_time=False):
    if type(exel_date) == str:
        if with_time:
            date = datetime.strptime(exel_date, '%d/%m/%Y %H:%M')
        else:
            date = datetime.strptime(exel_date, '%d/%m/%Y')
    elif type(exel_date) == float:
        date = str(xldate.xldate_as_datetime(exel_date, 0))
    return date


def upload_meter_data(meter, data_file, commodity):
    apps.get_model("data", "MeterRecord")
    stream = StringIO()
    writer = csv.writer(stream, delimiter='\t')
    wb = open_workbook(data_file.name)
    print('sheets = ', wb.nsheets, wb.sheets())
    for sheet in wb.sheets():
        # print('nr of collomns === ', sheet.ncols)
        # print('nr of rows === ', sheet.nrows)
        origin = get_origin(sheet.cell_value(3, 4))
        freq = get_frequency(sheet.cell_value(3, 2))
        # print('sheet = ', sheet.name, freq, origin)
        sheet_meter = {}
        meter_numbers = list(set(sheet.col_values(0, 6)))
        for number in meter_numbers:
            mmeter, created = Meter.objects.get_or_create(
                number=number, commodity=commodity)
            sheet_meter[number] = mmeter
        row_values = sheet.row_values(7)
        row_values = list(filter(None, row_values))
        if freq == "yearly":
            get_yearly(sheet, freq, origin, sheet_meter, writer)
        else:
            if len(row_values) == 3:
                get_daily(sheet, freq, origin, sheet_meter, writer)
            else:
                get_row_values(sheet, freq, origin, sheet_meter, writer)
        # file_values.append(sheet_values)
        # MeterRecord.objects.bulk_create(sheet_values)
    # writer.writerows(file_values)
    upload_data(stream)


def get_row_values(sheet, freq, origin, sheet_meter, writer):
    apps.get_model("data", "MeterRecord")
    for row in range(6, sheet.nrows):
        number = sheet.cell_value(row, 0)
        date = extract_date(sheet.cell_value(row, 1))
        row_values = sheet.row_values(row)
        row_values = list(filter(None, row_values))
        dates = generate_hh(date)
        hh_value = list(zip(dates, row_values[3:]))
        for hour, value in hh_value:
            writer.writerow([hour, float(value), freq,
                             origin, sheet_meter[number].id])


def get_daily(sheet, freq, origin, sheet_meter, writer):
    apps.get_model("data", "MeterRecord")
    for row in range(6, sheet.nrows):
        number = sheet.cell_value(row, 0)
        value = sheet.cell_value(row, 2)
        date = extract_date(sheet.cell_value(row, 1))
        writer.writerow([date, float(value), freq,
                         origin, sheet_meter[number].id])


def get_yearly(sheet, freq, origin, sheet_meter, writer):
    apps.get_model("data", "MeterRecord")
    for row in range(6, sheet.nrows):
        number = sheet.cell_value(row, 0)
        value = sheet.cell_value(row, 2)
        year = int(sheet.cell_value(row, 1))
        date = datetime(year, 1, 1, 0, 0)
        writer.writerow([date, float(value), freq,
                         origin, sheet_meter[number].id])


def get_origin(exel_origin):
    mtype = "actual"
    if exel_origin.lower() == "actual":
        mtype = "actual"
    else:
        mtype = "forcasted"
    return mtype


def get_frequency(frec):
    mtype = "daily"
    if frec.lower() == "day":
        mtype = "daily"
    elif frec.lower() == "hh":
        mtype = "hh"
    elif frec.lower() == "month":
        mtype = "monthly"
    elif frec.lower() == "week":
        mtype = "weekly"
    elif frec.lower() == "yearly":
        mtype = "yearly"
    return mtype


def generate_hh(day):
    hours = []
    hours.append(day)
    for i in range(47):
        day += timedelta(minutes=30)
        hours.append(day)
    return hours
