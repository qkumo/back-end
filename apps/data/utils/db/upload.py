from datetime import datetime, timedelta, time
from django.db.models import Q, Func, Sum, Max, Count
from contextlib import closing
from django.db import connection

def upload_data(stream):
    stream.seek(0)
    with closing(connection.cursor()) as cursor:
        cursor.copy_from(
            file=stream,
            table='data_meterrecord',
            sep='\t',
            columns=('date', 'value', 'mtype', 'origin_type', 'meter_id'),
        )
