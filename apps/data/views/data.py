from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework.views import APIView

from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from apps.data.models import *
from apps.data.serializers.data import *
from apps.data.models import *


class MeterDataFileView(APIView):
    authentication_classes = (JWTAuthentication, )
    permission_classes = (IsAuthenticated,)
    """
    Meter Data File Upload.
    """

    def post(self, request, format=None):
        serializer = MeterDataFileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
