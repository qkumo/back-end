from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from apps.data.models import Commodity
from apps.data.serializers import CommoditySerializer


class CommodityViewSet(viewsets.ModelViewSet):
    queryset = Commodity.objects.all()
    serializer_class = CommoditySerializer
    permission_classes = [IsAuthenticated]
