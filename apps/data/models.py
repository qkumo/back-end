from __future__ import unicode_literals

import os
from datetime import datetime

from django.db import models, transaction

from apps.commons.models import BaseModel
from apps.data.utils.file.read import upload_meter_data
from apps.sites.models import Site


class Commodity(BaseModel):
    name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    site = models.ForeignKey(Site, related_name='commodities',
                             on_delete=models.DO_NOTHING, null=True)
    company = models.ForeignKey(
        'company.Company', null=True, blank=True,
        on_delete=models.CASCADE, related_name='commodities')

    class Meta:
        verbose_name_plural = 'Commodities'

    def __str__(self):
        return str(self.name)


class MeterRecord(BaseModel):
    ATYPE_CHOICES = (
        ('actual', 'Actual'),
        ('forcasted', 'Forcasted'),
    )
    TYPE_CHOICES = (
        ('hh', 'HH'),
        ('hour', 'Hour'),
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('monthly', 'Monthly'),
        ('yearly', 'Yearly'),
    )
    mtype = models.CharField(
        choices=TYPE_CHOICES, default='hh', max_length=100, null=True, blank=True)
    origin_type = models.CharField(
        choices=ATYPE_CHOICES, default='actual', max_length=100, null=True, blank=True)
    meter = models.ForeignKey('sites.Meter', on_delete=models.CASCADE)
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE, null=True,
                             blank=True)
    company = models.ForeignKey('company.Company', on_delete=models.CASCADE, null=True,
                                blank=True)
    value = models.FloatField(null=True, default=0)
    date = models.DateTimeField(default=None)

    def __str__(self):
        return str(self.meter.number)

    class Meta:
        verbose_name_plural = 'Meter Readings'


class MeterDataFile(BaseModel):
    site = models.ForeignKey('sites.Site', null=True,
                             on_delete=models.CASCADE)
    commodity = models.ForeignKey(
        'data.Commodity', null=True, on_delete=models.CASCADE)
    meter = models.ForeignKey('sites.Meter', null=True,
                              on_delete=models.CASCADE)
    file = models.FileField(upload_to="meter", blank=True, null=True)

    def save(self, *args, **kwargs):
        # if not self.pk:
        super(MeterDataFile, self).save(*args, **kwargs)
        self.process_file(self.file.file)

    @transaction.atomic
    def process_file(self, data_file):
        fileName, fileExtension = os.path.splitext(data_file.name)
        if (fileExtension.lower() == '.xlsx' or fileExtension.lower() == '.xls'):
            upload_meter_data(self.meter, data_file, self.commodity)
