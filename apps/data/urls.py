from django.urls import path

from apps.data.views.data import *


urlpatterns = [
    path('file/', MeterDataFileView.as_view(), name='meter-file'),
    # path('reports/', ReportByDashboardList.as_view(), name='reports'),
]
