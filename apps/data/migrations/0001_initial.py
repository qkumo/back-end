# Generated by Django 2.2.4 on 2020-03-01 10:11

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Commodity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('code', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='MeterDataFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(default=datetime.datetime.now)),
                ('file', models.FileField(blank=True, null=True, upload_to='meter')),
            ],
        ),
        migrations.CreateModel(
            name='MeterRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mtype', models.CharField(blank=True, choices=[('hh', 'HH'), ('hour', 'Hour'), ('daily', 'Daily'), ('weekly', 'Weekly'), ('monthly', 'Monthly'), ('yearly', 'Yearly')], default='hh', max_length=100, null=True)),
                ('origin_type', models.CharField(blank=True, choices=[('actual', 'Actual'), ('forcasted', 'Forcasted')], default='actual', max_length=100, null=True)),
                ('value', models.FloatField(default=0, null=True)),
                ('date', models.DateTimeField(default=None)),
            ],
        ),
    ]
