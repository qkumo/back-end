from rest_framework import serializers
from ..models import *


class MeterDataFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeterDataFile
        fields = ('id', 'site', 'commodity', 'meter', 'file')

