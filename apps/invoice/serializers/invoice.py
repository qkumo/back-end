from rest_framework import serializers
from apps.invoice.models import Invoice



class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ['id', 'number', 'name', 'invoice_date', 'billing_period',
            'status','quantity','unit','commodity','no_commodity','tot_ex_VAT','tot_inci_VAT']

class InvoiceCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = "__all__"

