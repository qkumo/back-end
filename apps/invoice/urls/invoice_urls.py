from rest_framework.routers import DefaultRouter

from apps.invoice.views.invoice_views import InvoiceViewSet

router = DefaultRouter()

router.register('', InvoiceViewSet, basename="invoice")

urlpatterns = router.urls