from datetime import datetime

from django.db import models

from apps.commons.models import BaseModel


class Invoice(BaseModel):
    number = models.CharField(max_length=100)
    name = models.CharField(max_length=255)
    invoice_date = models.DateTimeField()
    billing_period = models.DateTimeField(default=datetime.now)
    status = models.CharField(max_length=255)
    quantity = models.BigIntegerField()
    unit = models.CharField(max_length=255)
    commodity = models.BigIntegerField()
    no_commodity = models.BigIntegerField()
    tot_ex_VAT = models.BigIntegerField()
    tot_inci_VAT = models.BigIntegerField()

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return str(self.name)
