from rest_framework import serializers
from apps.dashboard.models import *


class DashboardDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dashboard
        fields = ['id', 'name', 'access', 'profile']


class DashboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dashboard
        fields = ['id', 'name', 'access', 'created', 'updated', 'profile']


class ChartTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChartType
        fields = ['id', 'name', 'code']
