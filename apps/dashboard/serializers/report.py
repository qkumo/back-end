from rest_framework import serializers
from apps.dashboard.models import *
from apps.dashboard.serializers.dashboard import *


class ReportCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportCategory
        fields = ['id', 'name', 'code']


class ReportIndicatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportIndicator
        fields = ['id', 'name', 'code']


class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = ['id', 'name', 'created', 'updated', 'dashboard', 'category',
                  'indicator', 'mtype', 'chart_type', 'date_from', 'date_top']


class ReportDataSerializer(serializers.ModelSerializer):
    dashboard = DashboardDataSerializer()
    category = ReportCategorySerializer()
    indicator = ReportIndicatorSerializer()
    chart_type = ChartTypeSerializer()

    class Meta:
        model = Report
        fields = ['id', 'name', 'created', 'updated', 'dashboard', 'category',
                  'indicator', 'mtype', 'chart_type', 'date_from', 'date_top']
