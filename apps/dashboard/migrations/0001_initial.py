# Generated by Django 2.2.4 on 2020-03-01 10:11

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('company', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChartType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('code', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Dashboard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('created', models.DateTimeField(default=datetime.datetime.now)),
                ('updated', models.DateTimeField(default=datetime.datetime.now)),
                ('description', models.CharField(blank=True, max_length=100, null=True)),
                ('access', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', max_length=100, null=True)),
                ('profile', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='company.Profile')),
            ],
        ),
        migrations.CreateModel(
            name='ReportCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('code', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ReportIndicator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('code', models.CharField(blank=True, max_length=100, null=True)),
                ('category', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='dashboard.ReportCategory')),
            ],
        ),
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(default=datetime.datetime.now)),
                ('updated', models.DateTimeField(default=datetime.datetime.now)),
                ('date_from', models.DateTimeField(default=datetime.datetime.now)),
                ('date_top', models.DateTimeField(default=datetime.datetime.now)),
                ('name', models.CharField(blank=True, default='', max_length=100, null=True)),
                ('mtype', models.CharField(blank=True, choices=[('hh', 'HH'), ('hour', 'Hour'), ('daily', 'Daily'), ('weekly', 'Weekly'), ('monthly', 'Monthly'), ('yearly', 'Yearly')], default='hh', max_length=100, null=True)),
                ('category', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='dashboard.ReportCategory')),
                ('chart_type', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='dashboard.ChartType')),
                ('dashboard', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='dashboard.Dashboard')),
                ('indicator', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='dashboard.ReportIndicator')),
            ],
        ),
    ]
