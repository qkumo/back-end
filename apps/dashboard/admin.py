from django.contrib import admin
from apps.dashboard.models import *

admin.site.register(Dashboard)
admin.site.register(ChartType)
admin.site.register(Report)
admin.site.register(ReportIndicator)
admin.site.register(ReportCategory)
