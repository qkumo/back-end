from django.core.paginator import Paginator


def pagination(self, request, snippets):
    page = self.request.query_params.get('page', 1)
    size = self.request.query_params.get('size ', 10)
    paginator = Paginator(snippets, size)
    try:
        data = paginator.page(page)
    except Exception as e:
        data = []
    result = {
        'data': data,
        'page': int(page),
        'size': int(size)
    }
    return result