from datetime import datetime

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.commons.models import BaseModel


class Dashboard(BaseModel):
    ACCESS_CHOICES = (
        ('public', 'Public'),
        ('private', 'Private'),
    )
    name = models.CharField(max_length=100)
    created = models.DateTimeField(default=datetime.now)
    updated = models.DateTimeField(default=datetime.now)
    description = models.CharField(max_length=100, blank=True, null=True)
    access = models.CharField(
        choices=ACCESS_CHOICES, default="public", max_length=100, null=True, blank=True)
    profile = models.ForeignKey(
        'company.Profile', null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.name)


class ReportCategory(BaseModel):
    name = models.CharField(max_length=100, null=True, blank=True)
    code = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Report Categories'

    def __str__(self):
        return str(self.name)


class ReportIndicator(BaseModel):
    name = models.CharField(max_length=100, null=True, blank=True)
    code = models.CharField(max_length=100, null=True, blank=True)
    category = models.ForeignKey(
        ReportCategory, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = 'Report Indicators'

    def __str__(self):
        return str(self.name)


class ChartType(BaseModel):
    name = models.CharField(max_length=100, null=True, blank=True)
    code = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return str(self.name)


class Report(BaseModel):
    CHART_CHOICES = (
        ('bar', 'Bar'),
    )
    TYPE_CHOICES = (
        ('hh', 'HH'),
        ('hour', 'Hour'),
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('monthly', 'Monthly'),
        ('yearly', 'Yearly'),
    )
    date_from = models.DateTimeField(default=datetime.now)
    date_top = models.DateTimeField(default=datetime.now)
    name = models.CharField(max_length=100, default="", null=True, blank=True)
    dashboard = models.ForeignKey(
        Dashboard, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(
        ReportCategory, null=True, on_delete=models.SET_NULL)
    indicator = models.ForeignKey(
        ReportIndicator, null=True, on_delete=models.SET_NULL)
    mtype = models.CharField(
        choices=TYPE_CHOICES, default='hh', max_length=100, null=True, blank=True)
    chart_type = models.ForeignKey(
        ChartType, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.name)
