from django.shortcuts import get_object_or_404
from django.http import Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from apps.dashboard.serializers.dashboard import *
from apps.company.models import *
from ..models import *


class DashboardList(APIView):
    authentication_classes = (JWTAuthentication, )
    permission_classes = (IsAuthenticated,)
    """
    List all Dashboards By Profile, or create a new Dashboard.
    """

    def get(self, request, format=None):
        pid = self.request.query_params.get('profile', None)
        profile = get_object_or_404(Profile, id=pid)
        snippets = Dashboard.objects.filter(profile=profile)
        serializer = DashboardSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = DashboardSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DashboardDetail(APIView):
    authentication_classes = (JWTAuthentication, )
    permission_classes = (IsAuthenticated,)
    """
    Retrieve, update or delete a Dashboard instance.
    """

    def get_object(self, pk):
        try:
            return Dashboard.objects.get(pk=pk)
        except Dashboard.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = DashboardSerializer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = DashboardSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
