from django.shortcuts import get_object_or_404
from django.http import Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from apps.dashboard.models import *
from apps.dashboard.serializers.report import *
from apps.dashboard.serializers.dashboard import *
from apps.company.models import *


class ReportCategoryList(APIView):
    authentication_classes = (JWTAuthentication, )
    permission_classes = (IsAuthenticated,)
    """
    List all Reports By Dashboard, or create a new Dashboard.
    """

    def get(self, request, format=None):
        snippets = ReportCategory.objects.all()
        serializer = ReportCategorySerializer(snippets, many=True)
        return Response(serializer.data)


class ReportIndicatorList(APIView):
    authentication_classes = (JWTAuthentication, )
    permission_classes = (IsAuthenticated,)
    """
    List all Reports By Dashboard, or create a new Dashboard.
    """

    def get(self, request, format=None):
        snippets = ReportIndicator.objects.all()
        serializer = ReportIndicatorSerializer(snippets, many=True)
        return Response(serializer.data)


class ChartTypeList(APIView):
    authentication_classes = (JWTAuthentication, )
    permission_classes = (IsAuthenticated,)
    """
    List all Reports By Dashboard, or create a new Dashboard.
    """

    def get(self, request, format=None):
        snippets = ChartType.objects.all()
        serializer = ChartTypeSerializer(snippets, many=True)
        return Response(serializer.data)
