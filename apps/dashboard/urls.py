from django.urls import path

from apps.dashboard.views.dashboard import *
from apps.dashboard.views.chart import *
from apps.dashboard.views.category import *

urlpatterns = [
    path('list/', DashboardList.as_view(), name='dashboard-list'),
    path('id/<int:pk>/', DashboardDetail.as_view(), name='dashboard-detail'),
    path('reports/', ReportByDashboardList.as_view(), name='report-list'),
    path('reports/<int:pk>/', ReportDetail.as_view(), name='report-id'),

    path('report-category/', ReportCategoryList.as_view(), name='report-category'),
    path('report-indicator/', ReportIndicatorList.as_view(),
         name='report-indicator'),
    path('chart-category/', ChartTypeList.as_view(), name='chart-category'),

]
