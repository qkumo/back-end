from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.generics import UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.views import APIView

from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import RefreshToken

from apps.auths.serializers import (
    PasswordChangeSerializer,
    PasswordResetSerializer,
    PasswordSetSerializer,
    UserUpdateSerializer,
    AvatarSerializer
)

from apps.auths.utility import UserFromUidb64Mixin, token_generator
from apps.commons.tasks import send_mail_to
from apps.auths.serializers import UserSerializer
from apps.auths.serializers import UserSerializer
from apps.company.constants import Inactive

from apps.company.models import *
from apps.company.serializers import *

User = get_user_model()


class UserPasswordChangeView(UpdateAPIView):
    serializer_class = PasswordChangeSerializer
    permission_classes = [IsAuthenticated, ]

    def get_object(self):
        return self.request.user

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'message': "Password Changed."},
                        status=status.HTTP_200_OK)


class UserUpdateView(UpdateAPIView):
    serializer_class = UserUpdateSerializer
    permission_classes = [IsAuthenticated, ]

    def get_object(self):
        return self.request.user

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        user = self.get_object()
        user.username = request.data.get('email')
        user.save()
        return Response(response.data)


class PasswordResetViewset(UserFromUidb64Mixin, GenericViewSet):
    """
    A class to send password reset link with email
    """
    permission_classes = []
    queryset = User.objects.all()

    def get_serializer(self, *args, **kwargs):
        if self.action == 'create':
            return PasswordResetSerializer(*args, **kwargs)
        else:
            return PasswordSetSerializer(*args, **kwargs)

    def create(self, request):
        """send reset mail"""
        user = self._get_user_from_email(request.data.get('email'))
        if user:
            if user.is_active:
                # generate token and user_id for active user
                uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
                token = token_generator.make_token(user)
                frontend_reset_link, backend_reset_link = self.send_mail(
                    user, uidb64, token)
                data = {
                    'message':
                    _("A password reset link will be sent to your email address.")
                }
                if settings.DEBUG:
                    data.update({
                        'front_end_reset_link': frontend_reset_link,
                        'backend_reset_link': backend_reset_link
                    })
                return Response(data)
            return Response({
                'message': _('Inactive user. Please contact customer support!')
            }, status=status.HTTP_400_BAD_REQUEST)
        return Response({
            'message': _('User does not exist with provided email addreess!')
        }, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post', 'get'],
            detail=False,
            url_path=r'set/(?P<uidb64>[^/.]+)/(?P<token>[^/.]+)',
            url_name='set-password')
    def set_password(self, request, uidb64, token):
        """ set password """
        user = self._get_user_from_uidb64(uidb64)

        if user.is_active and token_generator.check_token(user, token):
            if request.method == 'GET':
                return Response(status=200)
            data = {
                'user': user.id,
                'password': request.data.get('password'),
            }
            serializer = PasswordSetSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            data = serializer.data
            data['message'] = "Successfully changed Password!"
            return Response(data)
        else:
            return Response({
                "error": "User not found"
            }, status=status.HTTP_404_NOT_FOUND)

    def send_mail(self, user, uidb64, token):
        """
        Send reset mail
        :param email: email to send reset link
        :param uidb64: userid encoded in base64
        :param token: reset token
        :param lang: email content language
        :return:
        """
        subject = _("Enkumo Password Reset Link")

        backend_reset_link = settings.FORGOT_PASSWORD_BE_URL + \
            "/api/auth/password-reset/set/" + uidb64 + "/" + token + "/"
        frontend_reset_link = settings.FORGOT_PASSWORD_FE_URL + \
            "/password-reset/" + uidb64 + "/" + token + "/"
        name = user.first_name if user.first_name else user.email

        context = {
            "name": name,
            "frontend_reset_link": frontend_reset_link,
            "backend_reset_link": backend_reset_link
        }

        message = """
        Dear {0},

        You have requested to reset your password. Click the link
        {1}
        to reset your password.

        If you did not request for your password reset, please ignore this message.

        Enkumo

        """.format(context.get('name'), frontend_reset_link)
        sender = getattr(settings, 'EMAIL_PASSWORD_RESET_USER',
                         'no-reply@enkumo.com')

        receivers = [user.email]
        send_mail_to.delay(
            subject,
            message,
            sender,
            receivers
        )
        return frontend_reset_link, backend_reset_link

    @ staticmethod
    def _get_user_from_email(email):
        """get user for given email"""
        if not email:
            raise ValidationError({"email": ["This field is required"]})
        try:
            user = User.objects.get(email=email)
        except (User.DoesNotExist, ValueError):
            return None
        return user


class ProfileAuthView(APIView):

    def get_tokens_for_user(self, user):
        refresh = RefreshToken.for_user(user)
        serializer = UserSerializer(user).data
        avatar = None
        if user.profile and user.profile.avatar:
            avatar = user.profile.avatar.url
        serializer['avatar'] = self.request.build_absolute_uri(
            avatar) if avatar else None
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
            'user': serializer
        }

    def post(self, request, format=None):
        email = request.data.get("email")
        password = request.data.get("password")

        if not email or not password:
            message = "Email and Password required!"
            return Response({"message": message}, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(username=email, password=password)

        if user:
            try:
                profile = user.profile
                if profile.company.status == Inactive:
                    message = "Company is inactive. Please contact customer support!"
                    return Response(
                        {"message": message},
                        status=status.HTTP_403_FORBIDDEN
                    )

            except Profile.DoesNotExist:
                message = "User is not associated with any company!"
                return Response({"message": message}, status=status.HTTP_401_UNAUTHORIZED)

            response = self.get_tokens_for_user(user)
            return Response(response, status=status.HTTP_200_OK)

        message = "Invalid login Credentials"
        return Response({"message": message}, status=status.HTTP_400_BAD_REQUEST)


class CustomRefreshTokenView(TokenRefreshView):

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        data = response.data
        access = data.get('access')

        jwt_object = JWTAuthentication()
        validated_token = jwt_object.get_validated_token(access)
        user = jwt_object.get_user(validated_token)

        refresh = RefreshToken.for_user(user)
        data.update({'refresh': str(refresh)})

        return Response(data)


class UserAvatarChangeView(UpdateAPIView):
    serializer_class = AvatarSerializer
    permission_classes = [IsAuthenticated, ]

    def get_object(self):
        return self.request.user.profile

    def delete(self, request):
        profile = self.get_object()
        if not profile.avatar:
            message = 'Avatar Not Found!'
            return Response({'message': message}, status=status.HTTP_404_NOT_FOUND)
        profile.avatar = None
        profile.save()
        message = 'Avatar Removed !'
        return Response({'message': message}, status=status.HTTP_204_NO_CONTENT)
