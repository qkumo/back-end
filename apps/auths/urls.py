from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView)

from apps.auths.views import (
    PasswordResetViewset, UserPasswordChangeView,
    UserUpdateView,
    ProfileAuthView,
    CustomRefreshTokenView,
    UserAvatarChangeView
)

app_name = 'auth'

router = DefaultRouter()

router.register(r'password-reset', PasswordResetViewset)

urlpatterns = [
    path('avatar/', UserAvatarChangeView.as_view(),
         name='avatar'),
    path('change-password/', UserPasswordChangeView.as_view(),
         name='change-password'),
    path('update-user/', UserUpdateView.as_view(),
         name='update-user'),
    path(r'login/', ProfileAuthView.as_view(), name='token_obtain_pair'),
    path(r'refresh/', CustomRefreshTokenView.as_view(), name='token_refresh'),
]
urlpatterns += router.urls
