from django.db.models import Q
from django.contrib.auth import get_user_model, login
from django.contrib.auth.password_validation import validate_password

from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.validators import UniqueValidator

from apps.company.models import Profile
from apps.company.serializers import CompanySerializer

User = get_user_model()


class DummyObject:

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class UserUpdateSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(
        max_length=225,
        allow_blank=False,
        validators=[
            UniqueValidator(queryset=User.objects.all())
        ])
    first_name = serializers.CharField(
        max_length=225,
        allow_blank=False
    )
    last_name = serializers.CharField(
        max_length=225,
        allow_blank=False
    )

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name']


class PasswordChangeSerializer(serializers.Serializer):
    """
    Serializer used for password change
    """
    old_password = serializers.CharField(
        max_length=128, write_only=True,
        style={'input_type': 'password'})
    password = serializers.CharField(
        max_length=128, write_only=True,
        style={'input_type': 'password'},
        min_length=8
    )

    confirm_password = serializers.CharField(
        max_length=128, write_only=True,
        style={'input_type': 'password'},
        min_length=8
    )

    def validate(self, attrs):
        request = self.context.get('request')
        if not request and request.user:
            raise ValidationError({'message': 'Some error Occured!'})
        password = attrs.get('password')
        confirm_password = attrs.get('confirm_password')
        if password != confirm_password:
            raise ValidationError(
                {'confirm_password': 'Password do not matched!'})

        return attrs

    def validate_old_password(self, old_password):
        request = self.context.get('request')
        if not request.user.check_password(old_password):
            raise ValidationError('Current password is incorrect!')
        return old_password

    def validate_password(self, password):
        """validate password using django's password validator"""
        validate_password(password)
        return password

    def create(self, validated_data):
        request = self.context.get('request')
        instance = request.user
        instance.set_password(validated_data.get('password'))
        instance.save()
        return DummyObject(user=instance, status="Successfully changed password")

    def update(self, instance, validated_data):
        pass


class PasswordResetSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', )


class PasswordSetSerializer(serializers.Serializer):
    """
    Serializer used for password set
    """
    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), write_only=True)
    password = serializers.CharField(max_length=128, write_only=True,
                                     style={'input_type': 'password'})

    def validate_password(self, password):
        """validate password using django's password validator"""
        validate_password(password)
        return password

    def create(self, validated_data):
        user = validated_data.get('user')
        user.set_password(validated_data['password'])
        user.save()
        return DummyObject(**validated_data)


class UserSerializer(serializers.ModelSerializer):
    company = serializers.SerializerMethodField()
    role = serializers.CharField(source="profile.role")

    class Meta:
        model = User
        fields = [
            'id',
            'email',
            'first_name',
            'last_name',
            'company',
            'role',
        ]

    def get_company(self, obj):
        try:
            profile = obj.profile
            return CompanySerializer(
                profile.company,
                context=self.context
            ).data

        except Profile.DoesNotExist:
            company = ""
            return company


class AvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['avatar']
