import six
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.http import Http404
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode

from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework import status

User = get_user_model()


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        login_timestamp = '' if user.last_login is None else user.last_login
        return (
            six.text_type(user.pk) + six.text_type(timestamp) + six.text_type(user.is_active) +
            six.text_type(login_timestamp)
        )


token_generator = TokenGenerator()


class UserFromUidb64Mixin:
    @staticmethod
    def _get_user_from_uidb64(uidb64):
        """get user from base64 encoded userid"""
        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist,
                ValidationError):
            return Response({"message": "User not Found!"},
                            status=status.HTTP_404_NOT_FOUND)
        return user
