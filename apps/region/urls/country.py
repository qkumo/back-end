
from django.urls import path

from apps.region.views import CountryAPIView

urlpatterns = [
    path('', CountryAPIView.as_view(), name="countries")

]
