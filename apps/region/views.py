from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from rest_framework.views import APIView
from rest_framework import generics

from apps.company.models import Profile
from apps.region.serializers.country import CountryDataSerializer
from apps.region.models import Country


class CountryAPIView(generics.ListAPIView):
    """
    Get countries for specific company user is associated with
    """
    permission_classes = (IsAuthenticated, )
    queryset = Country.objects.all()
    serializer_class = CountryDataSerializer

    def get_queryset(self):
        queryset = super().get_queryset()

        user = self.request.user
        try:
            profile = user.profile
            if not profile.company:
                return Response(
                    {"message": "No company is associated with user"},
                    status=status.HTTP_403_FORBIDDEN)
        except Profile.DoesNotExist:
            raise ValidationError({"message": "Profile Does Not Exist!"})

        company = profile.company
        sites = company.site_set.all()
        countries = [site.country.name for site in sites]
        queryset = Country.objects.filter(name__in=countries)
        return queryset
