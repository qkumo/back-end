from django.core.management import BaseCommand

from django_countries import countries
from apps.region.models import Country


class Command(BaseCommand):
    help = "Populate database with countries"

    def handle(self, *args, **options):
        item = 0
        for country in countries:
            Country.objects.get_or_create(
                code=country[0],
                defaults={'name': country[1]})
            item += 1
            print("Adding {}".format(country[1]))
        print("============Added {} countries=======".format(item))
