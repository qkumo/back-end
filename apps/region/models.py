from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_countries.fields import CountryField

from apps.commons.models import BaseModel

# Create your models here.


class Country(BaseModel):
    code = models.CharField(max_length=5, blank=True, null=True)
    name = models.CharField(max_length=101)

    class Meta:
        verbose_name_plural = 'Countries'

    def __str__(self):
        return str(self.name)


class City(BaseModel):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Cities'

    def __str__(self):
        return str(self.name)


class Plant(BaseModel):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)
