from rest_framework import serializers
from apps.region.models import *


class CountryDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['id', 'name']
