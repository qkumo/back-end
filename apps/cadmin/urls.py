from django.urls import path
from .views import *

app_name = 'cadmin'

urlpatterns = [
    path('', index, name='index'),
    path('logout/', logout_user, name='logout_user'),
    path('login/', login_user, name='login'),
    # company
    path('company-add',
         company_add, name='company_add'),
    path('company-edit/<int:pk>/', company_edit, name='company_edit'),

    # users
    path('company/<int:pk>/', company_home, name='home'),
    path('company/<int:pk>/account-add',
         company_user_add, name='company_user_add'),
    path('company/<int:company_pk>/account-edit/<int:pk>',
         company_user_edit, name='company_user_edit'),
    path('company/<int:company_pk>/change-password/<int:pk>',
         change_password_user, name='company_user_change_password'),
    path('company/<int:company_pk>/set-avatar/<int:pk>',
         set_user_avatar, name='set_avatar'),

    # sites
    path('company/<int:pk>/sites/',
         company_sites, name='company_sites'),
    path('company/<int:pk>/site-add/',
         company_site_add, name='company_site_add'),
    path('company/<int:company_pk>/site-edit/<int:pk>/',
         company_site_edit, name='company_site_edit'),

    # divisons
    path('company/<int:pk>/divisons/',
         company_divisions, name='company_divisions'),
    path('company/<int:pk>/divison-add/',
         company_division_add, name='company_division_add'),
    path('company/<int:company_pk>/divison-edit/<int:pk>/',
         company_divison_edit, name='company_divison_edit'),

    # commodities
    path('company/<int:pk>/commodities/',
         company_commodities, name='company_commodities'),
    path('company/<int:pk>/commodity-add/',
         company_commodity_add, name='company_commodity_add'),
    path('company/<int:company_pk>/commodity-edit/<int:pk>/',
         company_commodity_edit, name='company_commodity_edit'),

    # meters
    path('company/<int:pk>/meters/',
         company_meters, name='company_meters'),
    path('company/<int:pk>/meter-add/',
         company_meters_add, name='company_meter_add'),
    path('company/<int:company_pk>/meter-edit/<int:pk>/',
         company_meter_edit, name='company_meter_edit'),

    # fetch commodities
    path('company/<int:pk>/fetch_commodities/',
         commodity_fetch, name='fetch_commodities'),

    path('company/<int:pk>/master_upload/',
         master_upload, name='master_upload'),
    path('company/<int:pk>/custom_field/',
         custom_field_upload, name='custom_field'),

    # custom fields
    path('company/<int:pk>/custom-fields/',
         company_custom_fields, name='custom_fields'
         ),
    path('company/<int:pk>/custom-field-add/',
         company_custom_field_add, name='custom_field_add'),

    path('company/<int:pk>/consumtion_data/',
         consumtion_data_upload,
         name='consumtion_data_upload')

]
