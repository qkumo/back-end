from django.contrib import messages
from django.contrib.auth import get_user_model, logout
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import redirect, render

from apps.company.models import Company, Devision, Profile
from apps.sites.models import Site

from ..forms import *


@login_required
def company_divisions(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    divisons = Devision.objects.filter(company=company)

    context['pk'] = pk
    context['objects'] = divisons
    context['url'] = 'divison'
    context['company'] = company
    return render(request, 'cadmin/divisions/divisions.html', context)


@transaction.atomic
@login_required
def company_division_add(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    form = DivisonForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save(commit=False)
            instance.company = company
            instance.save()
            messages.success(request, 'Division Added Successfully.')
            return redirect('cadmin:company_divisions', pk)

    context['pk'] = pk
    context['form'] = form
    context['url'] = 'divison'
    context['company'] = company

    return render(request, 'cadmin/divisions/division_create.html', context)


@transaction.atomic
@login_required
def company_divison_edit(request, company_pk, pk):
    context = {}
    try:
        divison = Devision.objects.get(pk=pk)
    except Devision.DoesNotExist:
        messages.info(request, "No divison found")
        return redirect("cadmin:company_divisions", company_pk)

    form = DivisonForm(request.POST or None, instance=divison or None)
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save(commit=False)
            instance.company = divison.company
            instance.save()
            messages.success(
                request, 'Divison {} Edited Successfully.'.format(instance.name))
            return redirect('cadmin:company_divisions', company_pk)

    context['pk'] = company_pk
    context['company'] = Company.objects.get(pk=company_pk)
    context['form'] = form
    context['divison'] = divison
    context['url'] = 'divison'

    return render(request, 'cadmin/divisions/division_edit.html', context)
