from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import redirect, render
from pyexcel_xls import get_data as xls_get
from pyexcel_xlsx import get_data as xlsx_get

from apps.commons.models import CustomField, CustomFieldValue
from apps.company.models import Company
from apps.region.models import Country
from apps.sites.models import Site
from apps.data.models import MeterRecord

from ..forms import *

User = get_user_model()


@transaction.atomic
@login_required
def master_upload(request, pk):
    context = {}
    context['url'] = 'master_upload'
    context['pk'] = pk

    try:
        company = Company.objects.get(pk=pk)
        context['company'] = company
    except Company.DoesNotExist:
        messages.error(request, "Company Not Found.")

    if request.method == 'POST':

        if request.FILES['excel_file']:
            excel_file = request.FILES['excel_file']

            if (str(excel_file).split('.')[-1] not in ["xls", "xlsx"]):
                messages.warning(
                    request, "File type xlsx or xls required!")
                return redirect(request.path)

            if (str(excel_file).split('.')[-1] == "xls"):
                data = xls_get(excel_file)

            elif (str(excel_file).split('.')[-1] == "xlsx"):
                data = xlsx_get(excel_file)

            else:
                messages.error(
                    request, "Not valid file type requires excel format")

            sheet = data.get('setup')
            context['data'] = sheet

            for i, v in enumerate(sheet):

                if i == 0:
                    continue

                if len(sheet[i]) > 0:
                    meter = sheet[i][0]
                    commodity = sheet[i][1]
                    site = sheet[i][2]
                    division = sheet[i][3]
                    street = sheet[i][4]
                    city = sheet[i][5]
                    postal_code = sheet[i][6]
                    country = sheet[i][7]
                    supplier = ''
                    dno = ''
                    full_name = ''
                    email = ''
                    phone_number = ''
                    phone_number = ''
                    legal_entity_name = ''
                    if len(sheet[i]) > 8 and len(sheet[i]) < 15:
                        supplier = sheet[i][8]
                        dno = sheet[i][9]
                        full_name = sheet[i][10]
                        email = sheet[i][11]
                        phone_number = sheet[i][12]
                        legal_entity_name = sheet[i][13]

                    try:
                        divison = Devision.objects.get(
                            name=division,
                            company=company
                        )
                    except Devision.DoesNotExist:
                        divison = Devision.objects.create(
                            name=division,
                            company=company
                        )

                    try:
                        country = Country.objects.get(name=country)

                    except Country.DoesNotExist:
                        country = Country.objects.create(name=country)

                    try:
                        site = Site.objects.get(
                            name=site,
                            street=street,
                            city=city,
                            postal_code=postal_code,
                            country=country,
                            division=divison,
                            company=company,
                        )
                        site.dno = dno
                        site.supplier = supplier
                        site.legal_entity_name = legal_entity_name
                        site.save()
                    except Site.DoesNotExist:
                        site = Site.objects.create(
                            name=site,
                            street=street,
                            city=city,
                            postal_code=postal_code,
                            country=country,
                            division=divison,
                            company=company,
                            supplier=supplier,
                            dno=dno,
                            legal_entity_name=legal_entity_name

                        )
                    try:
                        commodity = Commodity.objects.get(
                            name=commodity,
                            site=site,
                            company=company
                        )
                    except Commodity.DoesNotExist:
                        commodity = Commodity.objects.create(
                            name=commodity,
                            site=site,
                            company=company
                        )

                    try:
                        meter = Meter.objects.get(
                            number=meter,
                            commodity=commodity,
                            site=site,
                            company=company
                        )

                    except Meter.DoesNotExist:
                        meter = Meter.objects.create(
                            number=meter,
                            commodity=commodity,
                            site=site,
                            company=company
                        )

                    if email and full_name and phone_number:
                        try:
                            user = User.objects.get(email=email)
                        except User.DoesNotExist:
                            name = full_name.split(' ')
                            user = User.objects.create(
                                email=email,
                                first_name=name[0],
                                last_name=name[-1],
                                username=email
                            )
                            password = "{}@123".format(name[0])
                            user.set_password(password)
                            user.save()
                        try:
                            profile = Profile.objects.get(user=user)
                            if not profile.site:
                                profile.site = site
                                profile.save()
                            if not profile.phone_number:
                                profile.phone_number = phone_number
                                profile.save()

                        except Profile.DoesNotExist:
                            profile = Profile.objects.create(
                                user=user,
                                company=company,
                                site=site,
                                phone_number=phone_number
                            )

            messages.success(request, "Setup success")

    return render(request, 'cadmin/master_upload.html', context)


def custom_field_upload(request, pk):
    context = {}
    context['url'] = 'custom_field_upload'
    context['pk'] = pk

    try:
        company = Company.objects.get(pk=pk)
        context['company'] = company
    except Company.DoesNotExist:
        messages.error(request, "Company Not Found.")
        return redirect('cadmin:index')

    if request.method == 'POST':
        if request.FILES.get('custom_field'):

            if (str(request.FILES.get('custom_field')).split('.')[-1] not in ["xls", "xlsx"]):

                messages.warning(
                    request, "File type xlsx or xls required!")
                return redirect(request.path)

            from django.contrib.contenttypes.models import ContentType
            ct = ContentType.objects.get_for_model(Site)

            import pandas as pd
            excel_file = request.FILES.get('custom_field')

            sheets = pd.ExcelFile(excel_file).sheet_names
            results = []

            for name in sheets:
                country = Country.objects.get(name=name)
                excel_data = pd.read_excel(excel_file, sheet_name=name)
                data = {'country': name, 'data': excel_data}
                results.append(data)
                for key, value in excel_data.items():

                    if key == "Site Name":
                        for val in value:
                            try:
                                site = Site.objects.get(
                                    name=val,
                                    company=company
                                )
                            except Site.DoesNotExist:
                                messages.warning(
                                    request, "The file you’re trying to upload contains one or more Sites that don’t exist, please make sure you create Sites first")
                                return redirect(request.path)

                    if not key in ["Site Name", "Meter", "Commodity"]:
                        custom_field, _ = CustomField.objects.get_or_create(
                            model=ct,
                            name=key,
                            company=company,
                            country=country
                        )

                        for val in value:
                            _, _ = CustomFieldValue.objects.get_or_create(
                                field=custom_field,
                                value=val,
                                object_id=site.pk
                            )

            messages.success(request, "Success.")
            context['results'] = results

    return render(request, 'cadmin/custom_field_upload.html', context)


@login_required
def consumtion_data_upload(request, pk):
    context = {}
    context['url'] = 'consumtion'
    context['pk'] = pk

    try:
        company = Company.objects.get(pk=pk)
        context['company'] = company
    except Company.DoesNotExist:
        messages.error(request, "Company Not Found.")
        return redirect('cadmin:index')

    if request.method == 'POST':
        commodity = request.POST.get('commodity')
        types = request.POST.get('type')
        frequency = request.POST.get('frequency')

        if request.FILES.get('file'):
            if (str(request.FILES.get('file')).split('.')[-1] not in ["xls", "xlsx"]):

                messages.warning(
                    request, "File type xlsx or xls required!")
                return redirect(request.path)

            import pandas as pd
            excel_file = request.FILES.get('file')

            sheets = pd.ExcelFile(excel_file).sheet_names
            results = []

            for name in sheets:
                try:
                    site = Site.objects.get(
                        name=name,
                        company=company
                    )
                except Site.DoesNotExist:
                    messages.warning(request, "Site doesnot exists")
                    return redirect(request.path)

                excel_data = pd.read_excel(excel_file, sheet_name=name)

                if frequency == 'monthly' and len(excel_data.columns) != 3:
                    messages.warning(
                        request, "Please upload monthly data.  Download sample")
                    return redirect(request.path)

                if frequency == 'hour' and len(excel_data.columns) < 24:
                    messages.warning(
                        request, "Please upload hourly data.  Download sample")
                    return redirect(request.path)

                data = {'site': name, 'data': excel_data}
                results.append(data)

                if frequency == 'hour':
                    for index in excel_data.index:
                        meter = excel_data['Meter'][index]
                        try:
                            meter = Meter.objects.get(number=meter)
                        except Meter.DoesNotExist:
                            messages.warning(request, "Meter DoesNotExist. ")
                            return redirect(request.path)

                        date = excel_data['Date'][index]
                        hour1 = excel_data['Consumption(KWh) H1'][index]
                        hour2 = excel_data['Consumption(KWh) H2'][index]
                        hour3 = excel_data['Consumption(KWh) H3'][index]
                        hour4 = excel_data['Consumption(KWh) H4'][index]
                        hour5 = excel_data['Consumption(KWh) H5'][index]
                        hour6 = excel_data['Consumption(KWh) H6'][index]
                        hour7 = excel_data['Consumption(KWh) H7'][index]
                        hour8 = excel_data['Consumption(KWh) H8'][index]
                        hour9 = excel_data['Consumption(KWh) H9'][index]
                        hour10 = excel_data['Consumption(KWh) H10'][index]
                        hour11 = excel_data['Consumption(KWh) H11'][index]
                        hour12 = excel_data['Consumption(KWh) H12'][index]
                        hour13 = excel_data['Consumption(KWh) H13'][index]
                        hour14 = excel_data['Consumption(KWh) H14'][index]
                        hour15 = excel_data['Consumption(KWh) H15'][index]
                        hour16 = excel_data['Consumption(KWh) H16'][index]
                        hour17 = excel_data['Consumption(KWh) H17'][index]
                        hour18 = excel_data['Consumption(KWh) H18'][index]
                        hour19 = excel_data['Consumption(KWh) H19'][index]
                        hour20 = excel_data['Consumption(KWh) H20'][index]
                        hour21 = excel_data['Consumption(KWh) H21'][index]
                        hour22 = excel_data['Consumption(KWh) H22'][index]
                        hour23 = excel_data['Consumption(KWh) H23'][index]
                        hour24 = excel_data['Consumption(KWh) H24'][index]

                        hours = [
                            hour1, hour2, hour3, hour4, hour5, hour6,
                            hour7, hour8, hour9, hour10, hour11,
                            hour12, hour13, hour14, hour15, hour16,
                            hour17, hour18, hour19, hour20, hour21,
                            hour22, hour23, hour24
                        ]

                        date = convert_date(date)
                        for i, v in enumerate(hours):
                            time = add_time(date, i)

                            MeterRecord.objects.create(
                                meter=meter,
                                site=site,
                                company=company,
                                value=v,
                                date=time,
                                mtype=frequency,
                                origin_type=types
                            )

                if frequency == 'monthly':
                    for index in excel_data.index:

                        meter = excel_data['Meter'][index]
                        date = excel_data['Date'][index]
                        date = convert_date(date)

                        consumtion = excel_data['Consumption (KWh)'][index]
                        try:
                            meter = Meter.objects.get(number=meter)
                        except Meter.DoesNotExist:
                            messages.warning(request, "Meter DoesNotExist")
                            return redirect(request.path)

                        MeterRecord.objects.create(
                            meter=meter,
                            site=site,
                            company=company,
                            value=consumtion,
                            date=date,
                            mtype=frequency,
                            origin_type=types
                        )

            context['results'] = results
            messages.success(request, "Consumption data uploaded successfully")

    return render(request, 'cadmin/consumtion/consumtion_monthly.html', context)


def convert_date(date):
    split = date.split("/")
    converted_date = "{}-{}-{}".format(
        split[2],
        split[1],
        split[0],
    )
    return converted_date


def add_time(date, hour):
    from dateutil.parser import parse
    time = "{} {}:{}:{}".format(
        date,
        hour,
        0,
        0
    )
    return parse(time)
