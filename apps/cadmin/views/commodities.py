from django.contrib import messages
from django.contrib.auth import get_user_model, logout
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import redirect, render

from apps.company.models import Company, Devision, Profile
from apps.sites.models import Site
from apps.data.models import Commodity

from ..forms import *


@login_required
def company_commodities(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")
    commodities = Commodity.objects.filter(site__company=company)
    sites = Site.objects.filter(company=company)

    if request.GET.get('site'):
        query = request.GET.get('site')
        site = Site.objects.get(pk=int(query))
        context['query'] = query
        context['site'] = site
        commodities = commodities.filter(site__pk=int(query))
    else:
        first_site = sites.first()
        context['first_site'] = first_site
        commodities = commodities.filter(site=first_site)

    context['sites'] = sites
    context['pk'] = pk
    context['url'] = 'commodity'
    context['objects'] = commodities
    context['company'] = company
    return render(request, 'cadmin/commodities/commodities.html', context)


@transaction.atomic
@login_required
def company_commodity_add(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    form = CommodityForm(request.POST or None)
    form.fields['site'].queryset = Site.objects.filter(company=company)
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save()
            instance.company = company
            instance.save()
            messages.success(request, 'Commodity Added Successfully.')
            return redirect('cadmin:company_commodities', pk)

    context['pk'] = pk
    context['url'] = 'commodity'
    context['form'] = form
    context['company'] = company

    return render(request, 'cadmin/commodities/commoditiy_add.html', context)


@transaction.atomic
@login_required
def company_commodity_edit(request, company_pk, pk):
    context = {}

    try:
        company = Company.objects.get(pk=company_pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    try:
        commodity = Commodity.objects.get(pk=pk)
    except Commodity.DoesNotExist:
        messages.info(request, "No commodity found")
        return redirect("cadmin:company_commodities", company_pk)

    form = CommodityEditForm(request.POST or None, instance=commodity or None)
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save()
            instance.site = commodity.site
            instance.company = company
            instance.save()
            messages.success(
                request, 'Commodity {} Edited Successfully.'.format(commodity.name))
            return redirect('cadmin:company_commodities', company_pk)

    context['pk'] = company_pk
    context['form'] = form
    context['commodity'] = commodity
    context['url'] = 'commodity'
    context['company'] = Company.objects.get(pk=company_pk)

    return render(request, 'cadmin/commodities/commodity_edit.html', context)
