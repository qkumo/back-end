from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth import get_user_model, logout
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import redirect, render

from apps.company.models import Company, Devision, Profile
from apps.sites.models import Site, Meter

from ..forms import *


@login_required
def company_meters(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    meters = Meter.objects.filter(commodity__site__company=company)

    context['pk'] = pk
    context['objects'] = meters
    context['url'] = 'meter'
    context['company'] = company
    return render(request, 'cadmin/meters/meters.html', context)


@transaction.atomic
@login_required
def company_meters_add(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    sites = company.site_set.all()
    form = MeterForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save()
            instance.site = instance.commodity.site
            instance.company = company
            instance.save()
            messages.success(request, 'Meter Added Successfully.')
            return redirect('cadmin:company_meters', pk)

    context['pk'] = pk
    context['form'] = form
    context['url'] = 'meter'
    context['sites_'] = sites
    context['company'] = company

    return render(request, 'cadmin/meters/meter_add.html', context)


@transaction.atomic
@login_required
def company_meter_edit(request, company_pk, pk):
    context = {}
    try:
        meter = Meter.objects.get(pk=pk)
    except Meter.DoesNotExist:
        messages.info(request, "No meter found")
        return redirect("cadmin:company_meters", company_pk)
    try:
        company = Company.objects.get(pk=company_pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:company_meters", company_pk)

    form = MeterEditForm(request.POST or None, instance=meter or None)
    form.fields['commodity'].queryset = Commodity.objects.filter(
        site=meter.commodity.site)

    if request.method == 'POST':
        if form.is_valid():
            instance = form.save()
            instance.site = instance.commodity.site
            instance.company = company
            instance.save()
            messages.success(
                request, 'Meter {} Edited Successfully.'.format(meter.number))
            return redirect('cadmin:company_meters', company_pk)

    sites = Site.objects.filter(company=company)

    context['sites_'] = sites
    context['pk'] = company_pk
    context['form'] = form
    context['meter'] = meter
    context['site'] = meter.commodity.site
    context['url'] = 'meter'
    context['company'] = Company.objects.get(pk=company_pk)

    return render(request, 'cadmin/meters/meter_edit.html', context)


def commodity_fetch(request, pk):
    try:
        site = Site.objects.get(pk=pk)
    except Site.DoesNotExist:
        return redirect('cadmin:index')

    commodities = list(Commodity.objects.filter(
        site=site, is_active=True).values('pk', 'name'))
    data = {"result": commodities}
    return JsonResponse(data, safe=False)
