from django.contrib import messages
from django.contrib.auth import get_user_model, logout
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.shortcuts import redirect, render

from apps.company.models import Company, Devision, Profile
from apps.sites.models import Site
from apps.commons.models import CustomField, CustomFieldValue

from ..forms import *


@login_required
def company_sites(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")
    sites = Site.objects.filter(company=company)

    context['pk'] = pk
    context['url'] = 'site'
    context['objects'] = sites
    context['company'] = company
    return render(request, 'cadmin/sites/sites.html', context)


@transaction.atomic
@login_required
def company_site_add(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    form = SiteForm(request.POST or None)
    form.fields['division'].queryset = Devision.objects.filter(company=company)

    content_type = ContentType.objects.get(app_label='sites', model='site')

    if request.method == 'POST':
        if form.is_valid():
            site = form.save(commit=False)
            site.company = company
            site.save()

            keys = request.POST.keys()
            for key in keys:
                if key not in ['csrfmiddlewaretoken', 'name', 'street', 'city',
                               'postal_code', 'country', 'division', 'supplier',
                               'dno', 'legal_entity_name']:
                    try:
                        custom_field = CustomField.objects.get(
                            name=key,
                            model=content_type,
                            company=company
                        )
                    except CustomField.DoesNotExist:
                        messages.warning(
                            request, "CustomField Does not Exist!")
                    CustomFieldValue.objects.get_or_create(
                        field=custom_field,
                        value=request.POST.get(key),
                        object_id=site.pk
                    )

            messages.success(request, 'Site Added Successfully.')
            return redirect('cadmin:company_sites', pk)

    context['pk'] = pk
    context['url'] = 'site'
    context['form'] = form
    context['company'] = company

    return render(request, 'cadmin/sites/site_create.html', context)


@transaction.atomic
@login_required
def company_site_edit(request, company_pk, pk):
    context = {}
    try:
        site = Site.objects.get(pk=pk)
    except Site.DoesNotExist:
        messages.info(request, "No site found")
        return redirect("cadmin:company_sites", company_pk)

    custom_field_values = CustomFieldValue.objects.filter(object_id=site.pk)
    content_type = ContentType.objects.get(app_label='sites', model='site')
    form = SiteEditForm(request.POST or None, instance=site or None)
    form.fields['division'].queryset = Devision.objects.filter(
        company=site.company)
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save(commit=False)
            instance.company = site.company
            instance.save()

            keys = request.POST.keys()
            for key in keys:
                if key not in ['csrfmiddlewaretoken', 'name', 'street', 'city',
                               'postal_code', 'country', 'division', 'supplier',
                               'dno', 'legal_entity_name']:
                    try:
                        custom_field = CustomField.objects.get(
                            name=key,
                            model=content_type,
                            company=site.company
                        )
                    except CustomField.DoesNotExist:
                        messages.warning(
                            request, "CustomField Does not Exist!")

                    cvalue, created = CustomFieldValue.objects.get_or_create(
                        field=custom_field,
                        object_id=site.pk,
                        defaults={'value': request.POST.get(key)}
                    )
                    if not created:
                        cvalue.value = request.POST.get(key)
                        cvalue.save()

            messages.success(
                request, 'Site {} Edited Successfully.'.format(instance.name))
            return redirect('cadmin:company_sites', instance.company.pk)

    context['pk'] = company_pk
    context['url'] = 'site'
    context['form'] = form
    context['site'] = site
    context['company'] = Company.objects.get(pk=company_pk)
    context['custom_field_values'] = custom_field_values

    return render(request, 'cadmin/sites/site_edit.html', context)


@login_required
def logout_user(request):
    logout(request)
    messages.success(request, "You have been logged out.")
    return redirect("cadmin:login")
