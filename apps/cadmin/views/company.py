from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import redirect, render


from apps.company.models import Company, Profile
from apps.sites.models import Site

from ..forms import *

# Create your views here.
User = get_user_model()


@login_required
def index(request):
    companies = Company.objects.all().order_by("-id")
    context = {"objects": companies}
    return render(request, 'cadmin/index.html', context)


@transaction.atomic
@login_required
def company_add(request):
    context = {}
    form = CompanyForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, 'Company Added Successfully.')
            return redirect('cadmin:index')

    context['form'] = form

    return render(request, 'cadmin/company_add.html', context)


@transaction.atomic
@login_required
def company_edit(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.error(request, "Company Not Found")
        return redirect('cadmin:index')

    form = CompanyForm(request.POST or None, instance=company)
    if request.method == 'POST':
        if form.is_valid():
            if form.has_changed():
                form.save()
                messages.success(request, 'Company Edited Successfully.')
            return redirect('cadmin:index')

    context['form'] = form
    context['company'] = company

    return render(request, 'cadmin/company_edit.html', context)


@login_required
def company_home(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")
    profiles = Profile.objects.filter(company=company).order_by('-id')

    context['company'] = company
    context['profiles'] = profiles
    context['url'] = 'profile'
    context['pk'] = pk
    return render(request, 'cadmin/company_home.html', context)


@transaction.atomic
@login_required
def company_user_add(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")
    form = UserRegisterForm(request.POST or None)
    form.fields['site'].queryset = company.site_set.all()
    if request.method == 'POST':
        if form.is_valid():
            user = form.save()
            role = form.cleaned_data.get('role')
            site = form.cleaned_data.get('site')
            phone_number = form.cleaned_data.get('phone_number')
            Profile.objects.create(
                company=company,
                user=user,
                role=role,
                site=site,
                phone_number=phone_number
            )
            messages.success(request, 'User Added Successfully.')
            return redirect('cadmin:home', pk)

    context['pk'] = pk
    context['form'] = form
    context['url'] = 'profile'
    context['company'] = company

    return render(request, 'cadmin/accounts/account_create.html', context)


@transaction.atomic
@login_required
def company_user_edit(request, company_pk, pk):
    context = {}
    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        messages.info(request, "No user found")
        return redirect('cadmin:company_home', company_pk)
    try:
        company = Company.objects.get(pk=company_pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect('cadmin:index')

    form = UserEditForm(request.POST or None, instance=user or None)
    profile_form = ProfileEditForm(
        request.POST or None, request.FILES or None,
        instance=user.profile
    )
    profile_form.fields['site'].queryset = company.site_set.all()

    if request.method == 'POST':
        if form.is_valid() and profile_form.is_valid():
            if form.has_changed():
                user = form.save()
            if profile_form.has_changed():
                profile_form.save()
            messages.success(
                request, f'User {user.email} Edited Successfully.')
            return redirect('cadmin:home', company_pk)

    context['pk'] = company_pk
    context['form'] = form
    context['profile_form'] = profile_form
    context['url'] = 'profile'
    context['user'] = user
    context['company'] = company

    return render(request, 'cadmin/accounts/accounts_edit.html', context)


@transaction.atomic
@login_required
def change_password_user(request, company_pk, pk):
    context = {}
    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        messages.info(request, "No user found")
        return redirect('cadmin:company_home', company_pk)

    form = ChangePasswordForm(request.POST or None, instance=user)

    if request.method == 'POST':
        if form.is_valid():
            if form.has_changed():
                form.save()
                messages.success(
                    request, f'Password changed for {user.email} .')
            return redirect('cadmin:home', company_pk)

    context['pk'] = company_pk
    context['form'] = form
    context['url'] = 'profile'
    context['user'] = user
    context['company'] = Company.objects.get(pk=company_pk)

    return render(request, 'cadmin/accounts/change_password.html', context)


@transaction.atomic
@login_required
def set_user_avatar(request, company_pk, pk):
    context = {}
    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        messages.info(request, "No user found")
        return redirect('cadmin:company_home', company_pk)
    try:
        profile = user.profile
    except Profile.DoesNotExist:
        messages.info(request, "User profile does not exists")
        return redirect('cadmin:company_home', company_pk)

    form = ProfileAvatarForm(
        request.POST or None,
        request.FILES,
        instance=profile
    )

    if request.method == 'POST':
        if form.is_valid():
            if form.has_changed():
                form.save()
                messages.success(
                    request, f'Avatar set for {user.email} .')
            return redirect('cadmin:home', company_pk)

    context['pk'] = company_pk
    context['form'] = form
    context['url'] = 'profile'
    context['user'] = user
    context['profile'] = profile
    context['company'] = Company.objects.get(pk=company_pk)

    return render(request, 'cadmin/accounts/set_avatar.html', context)


def login_user(request):
    if request.user.is_authenticated:
        return redirect("cadmin:index")
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user is None:
            messages.warning(request, "Invalid login credentials")
            return redirect(request.path)

        if not user.is_superuser or not user.is_staff:
            messages.info(request, "Not authorized")
            return redirect(request.path)

        if user.is_active:
            login(request, user)
            return redirect("cadmin:index")

    return render(request, 'cadmin/login_user.html')
