from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.shortcuts import redirect, render

from apps.cadmin.forms import CustomFieldForm
from apps.commons.models import CustomField
from apps.company.models import Company
from apps.region.models import Country


@login_required
def company_custom_fields(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    custom_fields = CustomField.objects.filter(company=company)

    context['pk'] = pk
    context['objects'] = custom_fields
    context['url'] = 'custom_field'
    context['company'] = company
    return render(request, 'cadmin/customfield/custom_fields.html', context)


@transaction.atomic
@login_required
def company_custom_field_add(request, pk):
    context = {}
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        messages.info(request, "No company found")
        return redirect("cadmin:index")

    sites = company.site_set.all()
    form = CustomFieldForm(request.POST or None)
    countries = [site.country.name for site in sites]
    form.fields['country'].queryset = Country.objects.filter(
        name__in=countries)

    content_type = ContentType.objects.get(app_label='sites', model='site')

    if request.method == 'POST':
        if form.is_valid():
            instance = form.save(commit=False)
            instance.model = content_type
            instance.company = company
            instance.save()
            messages.success(request, 'Custom Field Added Successfully.')
            return redirect('cadmin:custom_fields', pk)

    context['pk'] = pk
    context['form'] = form
    context['url'] = 'custom_field'
    context['sites_'] = sites
    context['company'] = company

    return render(request, 'cadmin/customfield/custom_fields_add.html', context)
