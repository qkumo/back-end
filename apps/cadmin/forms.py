import re
from django import forms
from django.contrib.auth import get_user_model, password_validation

from apps.commons.models import CustomField
from apps.company.models import Company, Devision, Profile
from apps.data.models import Commodity
from apps.sites.models import Meter, Site
from apps.company.constants import USER_ROLE_CHOICES

from django.utils.translation import gettext as _

PHONE_NUMBER_REGEX = re.compile(r"^(([+]?\d{3})-?)?\d{7,10}$")

User = get_user_model()


class ChangePasswordForm(forms.ModelForm):
    """
    form to change user password
    """
    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder': '', 'required': 'required'}),
        strip=False,
    )

    confirm_password = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder': '', 'required': 'required'}),
        strip=False,
    )

    class Meta:
        model = User
        fields = ['password', 'confirm_password']

    def clean_confirm_password(self):
        password = self.cleaned_data['password']
        confirm_password = self.cleaned_data['confirm_password']

        if password and confirm_password and password != confirm_password:
            raise forms.ValidationError('Password mismatch')
        password_validation.validate_password(password, self.instance)
        return password

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserRegisterForm(forms.ModelForm):
    """
    Form to register a new user
    """
    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder': '', 'required': 'required'}),
        strip=False,
    )

    confirm_password = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder': '', 'required': 'required'}),
        strip=False,
    )
    first_name = forms.CharField(label="First Name")
    last_name = forms.CharField(label="Last Name")
    email = forms.EmailField(label="Email Address")
    role = forms.ChoiceField(choices=USER_ROLE_CHOICES)
    site = forms.ModelChoiceField(queryset=Site.objects.all(),
                                  required=False)
    phone_number = forms.CharField(label='Phone Number', required=False)

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email',
            'phone_number',
            'password',
            'confirm_password',
            'role',
            'site',
        ]
        widgets = {
            'email': forms.EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '',
                    'required': 'required',
                },
            ),
            'first_name': forms.EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '',
                    'required': 'required',
                },
            ),
            'last_name': forms.EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '',
                    'required': 'required',
                },
            ),

        }

    def clean_confirm_password(self):
        password = self.cleaned_data['password']
        confirm_password = self.cleaned_data['confirm_password']

        if password and confirm_password and password != confirm_password:
            raise forms.ValidationError('Password mismatch')
        password_validation.validate_password(password, self.instance)
        return password

    def clean_email(self):
        email = self.cleaned_data.get('email')
        user = User.objects.filter(email=email)
        if user.exists():
            raise forms.ValidationError("User with email already exists!")
        return email

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        email = self.cleaned_data['email']
        user.username = email
        user.is_active = True
        if commit:
            user.save()
        return user


class UserEditForm(forms.ModelForm):
    """
    Form to edit  new user
    """
    first_name = forms.CharField(label="First Name")
    last_name = forms.CharField(label="Last Name")

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
        ]


class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = [
            'id',
            'user',
        ]
        labels = {
            "user": "User"
        }


class ProfileEditForm(forms.ModelForm):
    phone_number = forms.CharField(label="Phone Number", required=False)

    class Meta:
        model = Profile
        fields = [
            'phone_number',
            'role',
            'site',
        ]

    def clean_phone_number(self):
        number = self.cleaned_data.get('phone_number')
        self.validate_phone_number(number)
        return number

    @staticmethod
    def validate_phone_number(number):
        phone_number = str(number)

        if not PHONE_NUMBER_REGEX.match(phone_number):
            raise forms.ValidationError(
                _('Phone Number format is not valid. Some examples of supported'
                  ' phone numbers are numbers are 9811111111, 08256666,'
                  ' 977-9833333333, +977-9833333333, 977-08256666')
            )

        return number


class ProfileAvatarForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['avatar']


class SiteForm(forms.ModelForm):

    class Meta:
        model = Site
        exclude = ["company"]
        labels = {
            'dno': 'DNO'
        }


class SiteEditForm(forms.ModelForm):

    class Meta:
        model = Site
        exclude = ["company", "country"]
        labels = {
            'dno': 'DNO'
        }


class DivisonForm(forms.ModelForm):

    class Meta:
        model = Devision
        exclude = ["company"]


class CommodityForm(forms.ModelForm):

    class Meta:
        model = Commodity
        fields = "__all__"

    def clean(self):
        clean = super().clean()
        name = self.cleaned_data.get('name')
        site = self.cleaned_data.get('site')
        commodity = Commodity.objects.filter(site=site, name=name)
        if commodity.exists():
            self.add_error('name', 'Commodity with Site {} already exists'.
                           format(site.name))

        return clean


class CommodityEditForm(forms.ModelForm):
    class Meta:
        model = Commodity
        fields = ["is_active"]


class MeterForm(forms.ModelForm):

    class Meta:
        model = Meter
        fields = ["number", "commodity"]

    def clean(self):
        clean = super().clean()
        number = self.cleaned_data.get('number')
        commodity = self.cleaned_data.get('commodity')

        meter = Meter.objects.filter(
            number=number, commodity=commodity, commodity__site=commodity.site)

        if meter.exists():
            self.add_error('number', 'Meter {} on Site {} already exists'.
                           format(number, commodity.site.name))

        return clean


class MeterEditForm(forms.ModelForm):

    class Meta:
        model = Meter
        fields = ["number", "commodity"]


class CompanyForm(forms.ModelForm):

    class Meta:
        model = Company
        fields = "__all__"

        widgets = {
            'expiry_date': forms.DateInput(
                attrs={
                    'class': 'datepicker',
                    "type": "Date"
                }
            )
        }


class CustomFieldForm(forms.ModelForm):
    class Meta:
        model = CustomField
        fields = ['id', 'name', 'field_type', 'country']
