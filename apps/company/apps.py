from django.apps import AppConfig


class CompanyConfig(AppConfig):
    name = 'apps.company'
    verbose_name = 'Accounts & Users'
