from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.commons.models import BaseModel
from apps.commons.utility import get_upload_path
from apps.company.constants import (STATUS_CHOICES, USER_ROLE_CHOICES,
                                    Inactive, SiteResponsibleUser)
from apps.data.models import Commodity
from apps.region.models import Country
from apps.sites.models import Meter, Site
from apps.commons.validators import validate_phone_number

# Create your models here.


class Company(BaseModel):
    name = models.CharField(max_length=100, null=True, default=None)
    address = models.CharField(max_length=100, default=None, null=True)
    country = models.CharField(max_length=100)
    expiry_date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES,
                              default=Inactive)
    logo = models.ImageField(
        upload_to='uploads/company/logo',
        blank=True,
        null=True
    )

    class Meta:
        verbose_name_plural = 'Companies'

    def __str__(self):
        return str(self.name)


class Profile(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(
        upload_to=get_upload_path,
        blank=True,
        null=True
    )
    company = models.ForeignKey(
        Company,
        null=True,
        on_delete=models.SET_NULL
    )
    role = models.CharField(
        max_length=50,
        choices=USER_ROLE_CHOICES,
        default=SiteResponsibleUser
    )
    site = models.ForeignKey(
        'sites.Site',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='profiles'
    )
    phone_number = models.CharField(
        null=True,
        blank=True,
        validators=[validate_phone_number],
        max_length=25,
    )

    class Meta:
        verbose_name_plural = 'Profiles'

    def __str__(self):
        return self.user.email

    @property
    def get_full_name(self):
        full_name = self.user.email
        if self.user.first_name:
            full_name = f"{self.user.firstname} {self.user.lastname}"
        return full_name


class Devision(BaseModel):
    name = models.CharField(max_length=100)
    company = models.ForeignKey(Company, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = 'Divisions'

    def __str__(self):
        return str(self.name)


class ProxySite(Site):
    class Meta:
        proxy = True
        verbose_name_plural = "Sites"


class ProxyMeter(Meter):
    class Meta:
        proxy = True
        verbose_name_plural = "Meters"


class ProxyUser(User):
    class Meta:
        proxy = True
        verbose_name_plural = "Users"


class ProxyCommodity(Commodity):
    class Meta:
        proxy = True
        verbose_name_plural = "Commodities"


class ProxyCountry(Country):
    class Meta:
        proxy = True
        verbose_name_plural = "Countries"
