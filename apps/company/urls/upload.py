from django.urls import path
from rest_framework.routers import DefaultRouter

from apps.company.views import *

urlpatterns = [
    path('upload-custom-field/', CustomFieldView.as_view(), name='custom'),
    path('get-custom-fields/', FetchCustomFieldView.as_view(),
         name='fetch-custom')
]
