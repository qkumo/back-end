from django.urls import path
from rest_framework.routers import DefaultRouter

from apps.company.views import *

router = DefaultRouter()
router.register(r'', DivisionViewSet,  basename='division')

urlpatterns = router.urls
