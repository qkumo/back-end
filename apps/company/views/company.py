from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from apps.commons.models import CustomField, CustomFieldValue
from apps.sites.models import Site

from apps.company.models import *
from apps.company.serializers import *

User = get_user_model()


class CustomFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomField
        fields = "__all__"


class FetchCustomFieldView(APIView):
    """
    Get custom fields based on country , company
    """
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        """get request"""
        query_params = self.request.GET.get("country")
        company = self.request.GET.get("company")
        if not company:
            raise ValidationError(
                {"message": "Company required!"}
            )

        try:
            company = Company.objects.get(pk=int(company))
        except Company.DoesNotExist:
            raise ValidationError(
                {"message": "Company DoesNotExist !"}
            )

        if not query_params:
            raise ValidationError(
                {"message": "Country is required"}
            )

        content_type = ContentType.objects.get(app_label='sites', model='site')
        try:
            country = Country.objects.get(pk=int(query_params))
        except Country.DoesNotExist:
            raise ValidationError(
                {"message": "Country Does Not Exist"}
            )

        custom_fields = CustomField.objects.filter(
            company=company,
            model=content_type,
            country=country
        )
        serializer = CustomFieldSerializer(custom_fields, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class ProfileDetails(APIView):
    """
    List all sites, or create a new site.
    """

    authentication_classes = (JWTAuthentication, )
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        pid = self.request.query_params.get('profile', None)
        profile = Profile.objects.filter(id=pid)
        serializer = ProfileSerializer(data=profile)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CustomFieldView(APIView):

    def post(self, request, format=None):
        from django.contrib.contenttypes.models import ContentType
        ct = ContentType.objects.get_for_model(Site)
        user = self.request.user
        company = user.profile.company

        import pandas as pd
        excel_file = request.FILES.get('custom_field')

        sheets = pd.ExcelFile(excel_file).sheet_names
        results = []
        for name in sheets:
            country = Country.objects.get(name=name)
            excel_data = pd.read_excel(excel_file, sheet_name=name)
            data = {'country': name, 'data': excel_data}
            results.append(data)
            for key, value in excel_data.items():

                if key == "Site Name":
                    site = Site.objects.get(
                        name=value[0],
                        company=company
                    )

                if not key in ["Site Name", "Meter", "Commodity"]:
                    custom_field, _ = CustomField.objects.get_or_create(
                        model=ct,
                        name=key,
                        company=company,
                        country=country
                    )

                    for val in value:
                        _, _ = CustomFieldValue.objects.get_or_create(
                            field=custom_field,
                            value=val,
                            object_id=site.pk

                        )

        return Response(results)
