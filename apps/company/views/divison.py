from rest_framework import filters, status, viewsets
from rest_framework.permissions import IsAuthenticated
from apps.company.models import Profile, Devision
from apps.company.serializers import DevisionSerializer, DevisionCreateSerializer


class DivisionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    queryset = Devision.objects.all()
    serializer_class = DevisionSerializer

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return super().get_serializer_class()
        return DevisionCreateSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        user = self.request.user
        try:
            profile = user.profile
            queryset = queryset.filter(company=profile.company)
        except Profile.DoesNotExist:
            queryset = queryset.none()

        return queryset
