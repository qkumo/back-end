from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.commons.models import CustomField

from .models import Profile, Company, Devision

User = get_user_model()


class ProfileSerializer(serializers.Serializer):
    class Meta:
        model = Profile
        fields = ['id', 'title', 'code', 'linenos', 'language', 'style']

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Profile.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.title = validated_data.get('title', instance.title)
        instance.code = validated_data.get('code', instance.code)
        instance.linenos = validated_data.get('linenos', instance.linenos)
        instance.language = validated_data.get('language', instance.language)
        instance.style = validated_data.get('style', instance.style)
        instance.save()
        return instance


class ProfileAuthSerializer(serializers.Serializer):

    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        credentials = {
            'email': email,
            'password': password
        }


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name', 'country', 'address', 'expiry_date', 'status']


class CompanyDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name', 'country', 'address', 'expiry_date', 'status']


class DevisionDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devision
        fields = ['id', 'name']


class DevisionSerializer(serializers.ModelSerializer):
    company = CompanySerializer(read_only=True)

    class Meta:
        model = Devision
        fields = "__all__"


class DevisionCreateSerializer(serializers.ModelSerializer):
    company = serializers.PrimaryKeyRelatedField(
        queryset=Company.objects.all())

    class Meta:
        model = Devision
        fields = ["name", "company"]
