Active, Trial, Inactive = 'Active', 'Trial', 'Inactive'

STATUS_CHOICES = (
    (Active, Active),
    (Trial, Trial),
    (Inactive, Inactive)
)

SuperUser, SiteResponsibleUser = 'Super User', 'Site Responsible User'

USER_ROLE_CHOICES = (
    (SuperUser, SuperUser),
    (SiteResponsibleUser, SiteResponsibleUser),
)
