from datetime import datetime, timedelta
from rest_framework import permissions
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.contrib.auth.models import User
from apps.company.models import *


class PersonalDataPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        user_id = request.query_params.get('profile', None)
        if not user_id:
            user_id = request.data.get('profile', None)
        jwt = JWTAuthentication()
        decoded = jwt.authenticate(request)
        try:
            return int(decoded['id']) == int(user_id)
        except Exception as e:
            return False


class LimitAccessPermission(permissions.BasePermission):
    # message = 'Your are blocked !!!'

    def has_permission(self, request, view):
        jwt = JWTAuthentication()
        decoded = jwt.authenticate(request)
        profile = Profile.objects.get(id=decoded['id'])
        if (datetime.now().replace(tzinfo=None) > profile.expirate.replace(tzinfo=None)):
            return False
        else:
            return True


class DisableOptionsPermission(permissions.BasePermission):
    """
    Global permission to disallow all requests for method OPTIONS.
    """

    def has_permission(self, request, view):
        if request.method == 'OPTIONS':
            return False
        return True
