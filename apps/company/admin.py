from django.contrib import admin
from django.contrib.auth.models import Group

from apps.company.models import Profile
from apps.data.models import Commodity
from apps.sites.models import Meter

from .models import *

admin.site.register([
    Profile,
    Devision,
    ProxyCountry
])


class SiteInline(admin.TabularInline):
    model = Site
    extra = 1


class ProfileInline(admin.TabularInline):
    model = Profile
    extra = 1


class CommodytInline(admin.TabularInline):
    model = Commodity
    extra = 1


class MeterInline(admin.TabularInline):
    model = Meter
    extra = 1


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'country',
                    'status', 'expiry_date', 'created_at')
    inlines = [ProfileInline, SiteInline]


@admin.register(ProxySite)
class ProxySiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'company', 'country', 'division')
    inlines = [CommodytInline]


@admin.register(ProxyCommodity)
class ProxyCommodityAdmin(admin.ModelAdmin):
    list_display = ('name', 'site')
    inlines = [MeterInline]


@admin.register(ProxyMeter)
class ProxyMeterAdmin(admin.ModelAdmin):
    list_display = ('number', 'commodity', 'hh')


admin.site.unregister(Group)
