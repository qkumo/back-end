from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(
        title="Enkumo API",
        default_version='v1',
        description="Enkumo Rest APIs",
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)
urlpatterns = [
    path('docs', schema_view.with_ui(
        'swagger',
        cache_timeout=0
    ), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui(
        'redoc',
        cache_timeout=0
    ), name='schema-redoc'),
    path(r'admin/', admin.site.urls),
    # Modules
    path('api/', include('apps.api.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('cadmin/', include('apps.cadmin.urls'))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
