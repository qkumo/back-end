# Start the server (it is necesary to have docker and docker-compose installed on your machine)

1. docker-compose run server python migrate  
2. docker-compose run server python collectstatic --noinput  
3. docker-compose run server python createsuperuser  
4. docker-compose up
